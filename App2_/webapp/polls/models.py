from django.db import models


class User(models.Model):
    nombre_usuario = models.CharField(max_length=40)
    apellido= models.CharField(max_length=40)
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=12)
    descripcion = models.TextField()
    isAdmin = models.BooleanField()
    fecha_creacion = models.DateTimeField()
    fecha_modificacion = models.DateField(auto_now=True)


class Nacimientos(models.Model):
    nombre_apellidos = models.CharField(max_length = 100)
    cui = models.IntegerField()
    nacimiento = models.CharField(max_length = 60)
    genero = models.CharField(max_length=10)
    madre = models.CharField(max_length =100)
    padre = models.CharField(max_length =100)