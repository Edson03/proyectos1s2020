from django.shortcuts import render
from datetime import datetime

def datos(request):
    nombre = request.GET['nombre']    #Recibir el objeto
    nombre = nombre.split(',')        #Separar los elementos
    
    posts = [
        {
        'name': nombre,
        'title': 'Carné USAC',
        'type' : 'Estudiante',
        'id' : '300261320101',
        'numero' : '201611580',
        'date': datetime.now(),
        'image': 'https://cdn.mos.cms.futurecdn.net/2NBcYamXxLpvA77ciPfKZW-650-80.jpg.webp',
        }
    ]
    
    context = {
        'posts' : posts, 
    }
    return render(request,'Page.html', context)

# http://localhost:8000/datos/?nombre=Edson Andre,Martinez Alvarado


#   def saludo(request):
#   Tomar la hora exacta del suceso (TIMESTAMP)
#   IMPRIMIR EL NOMBRE COMO PARAMETRO
#   IMAGEN CON EL ATRIBUTO URL DE TIPO STRING
#   MOSTRAR COMO UN CARNET 
#   (foto) (nombre, dpi)
#   return HttpResponse(objeto)