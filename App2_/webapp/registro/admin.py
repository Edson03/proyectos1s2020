from registro.models import UsuarioRegistrado
from django.contrib import admin

# Register your models here.
@admin.register(UsuarioRegistrado)
class UsuarioAdmin(admin.ModelAdmin):
    list_display =('Usuario','Nombre','Creado','Telefono')

