from django.contrib.auth.models import User
from django.db import models
from django.contrib.admin import widgets

# Create your models here.
#Modelo extendido o Modelo proxy, para uso del modulo admin de django
#Clase Abstracta = Clase que contiene un model (Modelo del objeto que quiere recibir)
#No puede modificarse su estructura y todos los campos se vuelven obligatorios

class UsuarioRegistrado(models.Model):
    Usuario = models.OneToOneField(User,on_delete=models.CASCADE,blank=False)
    Nombre=models.CharField(max_length=100, blank=False,null=False)
    Edad = models.IntegerField()
    Nit = models.CharField(max_length=8, blank=False,null=False)
    Telefono = models.CharField(max_length=9, blank=True)
    Foto = models.ImageField(upload_to="registro/imagenes", blank=True, null=True)
    Email = models.EmailField(max_length = 200)
    Creado = models.DateTimeField(auto_now_add=True)
    Fecha_Nacimiento = models.DateField()
    SitioWeb = models.URLField(max_length = 50, blank=True) 
    Objeto = models.TextField(blank=True)

    def __str__(self):
        return self.user.username